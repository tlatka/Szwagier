package com.tlatka.szwagier;

import android.app.Activity;
import android.content.Intent;

public class Game {

    private Activity m_App;
    private LayoutManager m_Layout;
    private Swallow m_Swallow;
    private BeerGame m_BeerGame;
    private int m_Level;

    public Game(Activity a_App) {
        m_App = a_App;
        m_Layout = new LayoutManager(m_App);
        m_Swallow = new Swallow(m_App, this);
        m_BeerGame = new BeerGame(m_App);
        ShowStartScreen();
    }

    public enum BeerResult {
        NOTHING,
        OK,
        FAIL
    }

    public void Start() {
        Reset();
        m_BeerGame.NextLevel(m_Level);
        ShowBeerGame();
    }

    public void CheckBeerResult() {
        switch(m_BeerGame.Check()) {
            case NOTHING:
                Common.ShowToast(m_App, "Wypij coś");
                break;
            case OK:
                Common.ShowToast(m_App, "Brawo.\nPijemy dalej");
                NextLevel();
                break;
            case FAIL:
                Common.ShowToast(m_App, "Źle! Jesteś pijany.");
                //ShowStartScreen();
                ShowScoresScreen();
                break;
        }
    }

    public void StartSwallowGame() {
        m_Swallow.Start();
    }

    public void SetAccelerometerPosition(float a_Value) {
        m_Swallow.SetAccelerometerPosition(a_Value);
    }

    public void SetSwallowGameResult(boolean a_Result) {
        if (a_Result == true) {
            Common.ShowToast(m_App, "Brawo.\nPijemy dalej");
            m_BeerGame.NextLevel(m_Level);
            ShowBeerGame();
        }
        else {
            Common.ShowToast(m_App, "Obaliłeś się.");
            //ShowStartScreen();
            ShowScoresScreen();
        }
    }

    private void NextLevel() {
        m_Level += 1;
        if (m_Level % 4 == 0) {
            ShowSwallowGame();
        }
        else {
            m_BeerGame.NextLevel(m_Level);
        }
    }

    private void ShowBeerGame() {
        m_Layout.HideStartScreen();
        m_Layout.HideSwallowGame();
        m_Layout.ShowBeerGame();
    }

    private void ShowStartScreen() {
        m_Layout.HideSwallowGame();
        m_Layout.HideBeerGame();
        m_Layout.ShowStartScreen();
    }

    private void ShowScoresScreen() {
        Intent myIntent = new Intent(m_App, ActivityHighScore.class);
        myIntent.putExtra("score", (float)m_Level / 2.0f);  // score in promiles
        m_App.startActivity(myIntent);
    }

    private void ShowSwallowGame() {
        m_Layout.HideBeerGame();
        m_Layout.HideStartScreen();
        m_Layout.ShowSwallowGame();
    }

    private void Reset() {
        m_Level = 0;
        m_BeerGame = new BeerGame(m_App);
    }
}
