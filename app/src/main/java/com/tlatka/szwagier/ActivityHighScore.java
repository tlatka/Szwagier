package com.tlatka.szwagier;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityHighScore extends AppCompatActivity {

    private HighScores m_HighScores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);

        m_HighScores = new HighScores(this);

        Intent intent = getIntent();
        final float userScore = intent.getFloatExtra("score", 0.0f);

        AlertDialog.Builder question = new AlertDialog.Builder(this);

        final EditText edittext = new EditText(this);
        question.setTitle("Szwagier");
        question.setMessage("Zapisać wynik?");
        question.setView(edittext);
        question.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Editable userName = edittext.getText();
                m_HighScores.Save(userName.toString(), userScore);
                showScores();
            }
        });
        question.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                showScores();
            }
        });

        question.show();
    }

    private void showScores() {
        String scores_list =  m_HighScores.getScoresList();
        ((TextView)findViewById (R.id.scores_text_view)).setText(scores_list);
    }
}


