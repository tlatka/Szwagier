package com.tlatka.szwagier;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

public class BackSound extends Service {

    MediaPlayer m_Player;
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        m_Player = MediaPlayer.create(this, R.raw.smiechawa);
        m_Player.setLooping(true);
        float leftVolume = 20.0f;
        float rightVolume = 20.0f;
        m_Player.setVolume(leftVolume, rightVolume);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        m_Player.start();
        return 1;
    }

    public IBinder onUnBind(Intent arg0) {
        return null;
    }

    @Override
    public void onDestroy() {
        m_Player.stop();
        m_Player.release();
    }
}