package com.tlatka.szwagier;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class HighScores {
    private Activity m_App;
    private SharedPreferences m_SharedPreferencesNames;
    private SharedPreferences m_SharedPreferencesScores;

    private int m_ScoresNum = 0;

    HighScores(Activity a_App) {
        m_App = a_App;
        m_SharedPreferencesNames = m_App.getSharedPreferences(m_App.getString(R.string.high_score_names_file_key), Context.MODE_PRIVATE);
        m_SharedPreferencesScores = m_App.getSharedPreferences(m_App.getString(R.string.high_score_scores_file_key), Context.MODE_PRIVATE);

        m_ScoresNum = m_SharedPreferencesScores.getInt("scoresNum", 0);
    }

    public void Save(String a_Name, float a_Score) {
        m_ScoresNum = m_SharedPreferencesScores.getInt("scoresNum", 0) + 1;

        //TODO position
        int a_Position = m_ScoresNum;

        SharedPreferences.Editor editor = m_SharedPreferencesNames.edit();
        editor.putString(String.valueOf(a_Position), a_Name);
        editor.commit();

        editor = m_SharedPreferencesScores.edit();
        editor.putFloat(String.valueOf(a_Position), a_Score);
        editor.putInt("scoresNum", m_ScoresNum);
        editor.commit();
    }

    public String getScoresList() {
        String scoresList = "";
        for (int position = 1; position <= m_ScoresNum; ++position) {
            scoresList += String.valueOf(position) + ".  "
                        + m_SharedPreferencesNames.getString(String.valueOf(position), "")
                        + m_SharedPreferencesScores.getFloat(String.valueOf(position), 0.0f);
        }
        return scoresList;
    }


}
