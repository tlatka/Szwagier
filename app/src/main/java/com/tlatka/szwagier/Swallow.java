package com.tlatka.szwagier;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;

import java.util.Random;
import java.util.Vector;

import static android.content.Context.SENSOR_SERVICE;


public class Swallow {
    private Activity m_App;
    private SensorManager m_SensorManager;
    private Game m_Game;
    private Random m_RandomGenerator;

    private float m_MaxRotation = 60.0f;
    private int m_IterationTime = 25;  //ms
    private int m_IterationMaxNum = 200;
    private int m_IterationCurrentNum;
    private float m_LastPictureRotation;

    private Vector<Float> m_AccelerometerPositions;

    ImageView m_SwallowImage;


    public Swallow(Activity a_App, Game a_Game) {
        m_App = a_App;
        m_Game = a_Game;
        m_SensorManager = (SensorManager) m_App.getSystemService(SENSOR_SERVICE);
        m_RandomGenerator = new Random();
        m_AccelerometerPositions = new Vector<>();
        m_SwallowImage = (ImageView)m_App.findViewById(R.id.image_jaskolka);
    }

    public void Start() {
        m_IterationCurrentNum = 0;
        m_LastPictureRotation = 0.0f;
        m_SensorManager.registerListener(
                (SensorEventListener)m_App,
                m_SensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);   // SENSOR_DELAY_GAME = 20ms
        ScheduleRandomMove();
    }

    public void SetAccelerometerPosition(float a_Value) {
        if (m_AccelerometerPositions.size() > 10) {
            m_AccelerometerPositions.remove(0);
        }
        m_AccelerometerPositions.add(a_Value);

        float sum = 0.0f;
        for (float value : m_AccelerometerPositions) {
            sum += value;
        }
        float accelerometerRotation = sum/m_AccelerometerPositions.size();

        m_LastPictureRotation = m_LastPictureRotation + accelerometerRotation;
        m_SwallowImage.setRotation(m_LastPictureRotation);
    }

    private void ScheduleRandomMove() {
        m_IterationCurrentNum++;

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                RotatePictureOnTime();
                if (!IsBallance()) {
                    SetSwallowGameResult(false);
                    return;
                }
                if (m_IterationCurrentNum >= m_IterationMaxNum) {
                    SetSwallowGameResult(true);
                } else {
                    ScheduleRandomMove();
                }

            }
        }, m_IterationTime);
    }

    private void RotatePictureOnTime() {
        float currentMove = m_RandomGenerator.nextFloat() * 2.0f;
        if (m_IterationCurrentNum > m_IterationMaxNum/2) {
            // on half change direction
            currentMove = currentMove * (-1.0f);
        }

        m_LastPictureRotation = m_LastPictureRotation + currentMove;
        m_SwallowImage.setRotation(m_LastPictureRotation);
    }

    private void SetSwallowGameResult(boolean a_Result) {
        m_SensorManager.unregisterListener((SensorEventListener)m_App);
        m_Game.SetSwallowGameResult(a_Result);
    }

    private boolean IsBallance() {
    return Math.abs(m_LastPictureRotation) <  m_MaxRotation;

    }
}
