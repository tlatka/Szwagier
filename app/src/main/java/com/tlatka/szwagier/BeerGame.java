package com.tlatka.szwagier;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Random;
import java.util.Vector;

public class BeerGame {

    private Activity m_App;
    private Random m_RandGen;
    private float m_Promil;

    private String m_CurrentBeer;
    private Vector<String> m_AllBeers;
    private Vector<String> m_BeersOnSingleLevel;

    public BeerGame(Activity a_App) {
        m_App = a_App;
        m_AllBeers = new Vector();
        InitBeerList();
        m_BeersOnSingleLevel = new Vector<String>();
        m_RandGen = new Random();
        m_Promil = 0.0f;
    }

    public void NextLevel(int a_Level) {
        SetPromil(a_Level);
        SetRadioButtons();
        SetCurrentBeer();
        SetNewBeerImage();
    }

    public Game.BeerResult Check() {
        RadioGroup radioGroup = (RadioGroup)m_App.findViewById(R.id.radio_group);
        int selectedId = radioGroup.getCheckedRadioButtonId();
        if (selectedId == -1) {
            return Game.BeerResult.NOTHING;
        }

        RadioButton userSelectedBeer = (RadioButton) m_App.findViewById(selectedId);
        if (userSelectedBeer.getText() == m_CurrentBeer) {
            return Game.BeerResult.OK;
        }
        else {
            return Game.BeerResult.FAIL;
        }
    }

    private void SetPromil(int a_Level) {
        m_Promil = (float)a_Level / 2.0f;
        TextView textPromil = (TextView)m_App.findViewById(R.id.text_promile_value);
        textPromil.setText(String.valueOf(m_Promil));
    }

    private void SetRadioButtons() {
        RandomBeerList();
        RadioGroup rbtnGrp = (RadioGroup)m_App.findViewById(R.id.radio_group);
        rbtnGrp.clearCheck();

        for (int i = 0; i < rbtnGrp.getChildCount(); i++) {
            ((RadioButton) rbtnGrp.getChildAt(i)).setText(m_BeersOnSingleLevel.get(i));
        }
    }

    private void RandomBeerList() {
        m_BeersOnSingleLevel.clear();
        Vector<String> allBeers = new Vector(m_AllBeers);

        for (int i = 0; i < 4; ++i) {
            int nextNum = m_RandGen.nextInt(allBeers.size());
            m_BeersOnSingleLevel.add(allBeers.get(nextNum));
            allBeers.remove(nextNum);
        }
    }

    private void SetCurrentBeer() {
        Random randGen = new Random();
        int beerNum = randGen.nextInt(4);
        m_CurrentBeer = m_BeersOnSingleLevel.get(beerNum);

        ImageView currentImage = (ImageView)m_App.findViewById(R.id.image_beer);
        int imageId = Common.beerList.get(m_CurrentBeer);
        currentImage.setImageResource(imageId);
    }

    private void SetNewBeerImage() {
        ImageView currentImage = (ImageView)m_App.findViewById(R.id.image_beer);
        BitmapDrawable drawable = (BitmapDrawable) currentImage.getDrawable();
        Bitmap bitmapOriginal = drawable.getBitmap();

        Bitmap bitmapBlured = Common.fastblur(bitmapOriginal, 1, (int)(1.0f + m_Promil * 10.0f));

        int currentBitmapWidth = bitmapBlured.getWidth();
        int currentBitmapHeight = bitmapBlured.getHeight();

        int newWidth = currentImage.getMeasuredWidth();
        int newHeight = (int) Math.floor((double) currentBitmapHeight *( (double) newWidth / (double) currentBitmapWidth));
        Bitmap bitmapResized = Bitmap.createScaledBitmap(bitmapBlured, newWidth, newHeight, true);

        currentImage.setImageBitmap(bitmapResized);
    }

    private void InitBeerList() {
        for (String beerName : Common.beerList.keySet()){
            m_AllBeers.add(beerName);
        }
    }
}
