package com.tlatka.szwagier;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

public class LayoutManager {
    private Activity m_App;

    private Button m_StartButton;
    private TextView m_StartText;

    private Button m_BeerGameWypijButton;
    private RadioGroup m_BeerGameRadioGroup;
    private TextView m_BeerGamePromileValue;
    private TextView m_BeerGamePromileName;
    private ImageView m_BeerGameImage;

    private TextView m_SwallowInfoText;
    private Button m_SwallowButton;
    private ImageView m_SwallowImage;


    public LayoutManager(Activity a_App) {
        m_App = a_App;

        m_StartButton = (Button)m_App.findViewById(R.id.button_start);
        m_StartText = (TextView)m_App.findViewById(R.id.text_start);
        m_StartText.setGravity(Gravity.CENTER);

        m_BeerGameWypijButton = (Button)m_App.findViewById(R.id.button_wypij);
        m_BeerGameRadioGroup = (RadioGroup)m_App.findViewById(R.id.radio_group);
        m_BeerGamePromileValue = (TextView)m_App.findViewById(R.id.text_promile_value);
        m_BeerGamePromileName = (TextView)m_App.findViewById(R.id.text_promile_name);
        m_BeerGameImage = (ImageView)m_App.findViewById(R.id.image_beer);

        m_SwallowInfoText = (TextView)m_App.findViewById(R.id.text_jaskolka);
        m_SwallowInfoText.setGravity(Gravity.CENTER);
        m_SwallowButton = (Button)m_App.findViewById(R.id.button_jaskolka);
        m_SwallowImage = (ImageView)m_App.findViewById(R.id.image_jaskolka);

    }

    public void ShowStartScreen() {
        m_StartButton.setVisibility(View.VISIBLE);
        m_StartText.setVisibility(View.VISIBLE);
    }
    public void HideStartScreen() {
        m_StartButton.setVisibility(View.INVISIBLE);
        m_StartText.setVisibility(View.INVISIBLE);
    }

    public void ShowBeerGame() {
        m_BeerGameWypijButton.setVisibility(View.VISIBLE);
        m_BeerGameRadioGroup.setVisibility(View.VISIBLE);
        m_BeerGamePromileValue.setVisibility(View.VISIBLE);
        m_BeerGamePromileName.setVisibility(View.VISIBLE);
        m_BeerGameImage.setVisibility(View.VISIBLE);
    }
    public void HideBeerGame() {
        m_BeerGameWypijButton.setVisibility(View.INVISIBLE);
        m_BeerGameRadioGroup.setVisibility(View.INVISIBLE);
        m_BeerGamePromileValue.setVisibility(View.INVISIBLE);
        m_BeerGamePromileName.setVisibility(View.INVISIBLE);
        m_BeerGameImage.setVisibility(View.INVISIBLE);
    }

    public void ShowSwallowGame() {
        m_SwallowInfoText.setVisibility(View.VISIBLE);
        m_SwallowButton.setVisibility(View.VISIBLE);
        m_SwallowImage.setVisibility(View.VISIBLE);
    }

    public void HideSwallowGame() {
        m_SwallowInfoText.setVisibility(View.INVISIBLE);
        m_SwallowButton.setVisibility(View.INVISIBLE);
        m_SwallowImage.setVisibility(View.INVISIBLE);
    }

}
