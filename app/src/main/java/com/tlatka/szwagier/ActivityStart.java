package com.tlatka.szwagier;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

import static android.R.attr.value;

public class ActivityStart extends AppCompatActivity implements SensorEventListener {

    private Game m_Game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        m_Game = new Game(this);

        // start
        Button buttonStart = (Button) findViewById(R.id.button_start);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_Game.Start();
            }
        });

        // game
        Button buttonGame = (Button) findViewById(R.id.button_wypij);
        buttonGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_Game.CheckBeerResult();
            }
        });

        //swallow
        final Button buttonAccel = (Button) findViewById(R.id.button_jaskolka);
        buttonAccel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAccel.setVisibility(View.INVISIBLE);
                m_Game.StartSwallowGame();
            }
        });
    }

    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            m_Game.SetAccelerometerPosition(event.values[0]);
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        startService(new Intent(this, BackSound.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, BackSound.class));
    }
}
